from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from .models import Posts


def post_list(request):
    posts = Posts.objects.all()
    return render(request, 'post_list.html', context={'post_list': posts})


def post_details(request, post_id):
    post = Posts.objects.get(pk=post_id)
    return render(request, 'post.html', context={'post': post})
