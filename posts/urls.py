from django.urls import path
from .views import *

urlpatterns = [
    path('<int:post_id>', post_details, name='post'),
    path('', post_list, name='post_list')
]