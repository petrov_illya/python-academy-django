from django.db import models
from posts_db import posts


class Posts(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_posts():
        for i in posts:
            post = Posts(title=i['title'], body=i['body'])
            post.save()



